/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.sheridancollege.softwarefundamentals.pickacard;

/**
 * A class that fills a magic hand of 7 cards with random Card Objects
 * and then asks the user to pick a card and searches the array of cards
 * for the match to the user's card. To be used as starting code in ICE 1
 * @author dancye
 */
import java.util.Scanner;

public class CardTrick {
    
    public static void main(String[] args){
        
        Scanner in = new Scanner(System.in);
        
        Card[] magicHand = new Card[7];
        
        for (int i = 0; i < magicHand.length; i++){
            
            magicHand[i] = new Card();
            
            magicHand[i].setValue((int)(Math.random()* 14) );
            
            magicHand[i].setSuit(Card.SUITS[(int)(Math.random() * 4)]);
        }
        
        Card[] cardPick = new Card[1];

        for(int i = 0; i < cardPick.length; i++){
            
            cardPick[0] = new Card();
            
            System.out.println("Pick a card value( 0 to 13):");
            cardPick[i].setValue(in.nextInt());
            
            System.out.println("Pick a card suit:");
            cardPick[i].setSuit(in.next());
        }
        
        int count = 0;
        
        for(int i = 0; i < magicHand.length; i++){
            if(cardPick[0].getValue() == magicHand[i].getValue() && 
                    cardPick[0].getSuit().equals(magicHand[i].getSuit())){
                            
               count++;
            } 
        }
        
        System.out.println("Your card was in the Magic Hand " + count + 
                " times.");
    }
}
